
# Managing Storage on Docker Servers and Registries.

The sample `worker` apps are adapted from Docker's [example-voting-app](https://github.com/dockersamples/example-voting-app).

The `dotnet` variant uses Windows containers. You can build and run it on Windows Server 2019 with [Docker Enterprise](https://hub.docker.com/editions/enterprise/docker-ee-server-windows) or [Docker Desktop for Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows) (in Windows containers mode).

The `java` variant uses Linux containers. You can build and run it with [Docker on Linux](https://hub.docker.com/search?q=&type=edition&offering=community), [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac) and [Docker Desktop for Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows) (in Linux containers mode).
