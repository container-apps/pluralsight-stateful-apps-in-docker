# Deploying Stateful Applications for High Availability with Docker Volumes  

Module 2 samples - stateful web apps used to demonstrate volume storage.

The sample apps `api-consumer` and `store` use Linux containers. 

You can build and run them with [Docker on Linux](https://hub.docker.com/search?q=&type=edition&offering=community), [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac) and [Docker Desktop for Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows) (in Linux containers mode).